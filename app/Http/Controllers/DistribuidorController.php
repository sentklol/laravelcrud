<?php

namespace App\Http\Controllers;

use App\Models\Distribuidor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DistribuidorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['distribuidores']=Distribuidor::paginate(5);
        return view('distribuidor.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('distribuidor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $campos=[
            'Nombre'=>'required|string|max:100',
            'Ciudad'=>'required|string|max:100',
            'Correo'=>'required|string|max:100',
            'Logo'=>'required|max:10000|mimes:jpeg,png,jpg'
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
            'Logo.required'=>'Se necesita insertar un logo'

        ];

        $this->validate($request, $campos,$mensaje);

        $datosDistribuidor = request()->except('_token');

        if($request->hasFile('Logo')){
            $datosDistribuidor['Logo']=$request->file('Logo')->store('uploads','public');
        }
        Distribuidor::insert($datosDistribuidor);

        return redirect('distribuidor')->with('mensaje','Distribuidor añadido');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Distribuidor $distribuidor
     * @return \Illuminate\Http\Response
     */
    public function show(Distribuidor $distribuidor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Distribuidor  $distribuidor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $distribuidor=Distribuidor::findOrFail($id);
        return view('distribuidor.edit', compact('distribuidor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Distribuidor  $distribuidor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[
            'Nombre'=>'required|string|max:100',
            'Ciudad'=>'required|string|max:100',
            'Correo'=>'required|string|max:100',  
        ];

        $mensaje=[
            'required'=>'El :attribute es requerido',
        ];

        if($request->hasFile('Logo')){
           $campos=['Logo'=>'required|max:10000|mimes:jpeg,png,jpg'];
           $mensaje=['Logo.required'=>'Se necesita insertar un Logo'];
        }

        $this->validate($request, $campos,$mensaje);

        $datosDistribuidor = request()->except(['_token','_method']);
        
        if($request->hasFile('Logo')){
            $distribuidor=Distribuidor::findOrFail($id);
            Storage::delete('public/'.$distribuidor->Logo);
            $datosDistribuidor['Logo']=$request->file('Logo')->store('uploads','public');
        }
        
        Distribuidor::where('id','=',$id)->update($datosDistribuidor);
        $distribuidor=Distribuidor::findOrFail($id);

        return redirect('distribuidor')->with('mensaje','Distribuidor editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Distribuidor  $distribuidor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $distribuidor=Distribuidor::findOrFail($id);
        if(Storage::delete('public/'.$distribuidor->Logo)){

        Distribuidor::destroy($id);
        }

        return redirect('distribuidor')->with('mensaje','Distribuidor borrado');
    }
}