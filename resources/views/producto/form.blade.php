<h1> {{$modo}} producto </h1>

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">
<ul>

        @foreach( $errors->all() as $error)
       <li> {{ $error }} </li>
    @endforeach
</ul>   

    </div>


@endif




<div class="form-group">
<label for="Nombre">  NOMBRE </label>
<input type="text" class="form-control" name="Nombre" value="{{isset($producto->Nombre)?$producto->Nombre:old('Nombre')}}" id="Nombre">
</div>

<div class="form-group">
<label for="Marca">  MARCA </label>
<input type="text" class="form-control" name="Marca" value="{{isset($producto->Marca)?$producto->Marca:old('Marca')}}" id ="Marca">
</div>

<div class="form-group">
<label for="Cantidad">  CANTIDAD </label>
<input type="text" class="form-control" name="Cantidad" value="{{isset($producto->Cantidad)?$producto->Cantidad:old('Cantidad')}}" id="Cantidad">
</div>

<div class="form-group">
<label for="Foto"></label>
@if(isset($producto->Foto))
<img class="img-thumbnail img-fluid" src="{{asset('storage').'/'.$producto->Foto }}" width="100" alt="">
@endif 
<input type="file" class="form-control" name="Foto" value="" id="Foto">
</div>

<input class="btn btn-success" type="submit" value="{{$modo}} datos">
<a class="btn btn-outline-info" href="{{url('producto/')}}">Regresar</a>

<br>
