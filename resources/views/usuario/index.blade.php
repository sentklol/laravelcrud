@extends('layouts.app')
@section('content')
<div class="container">

@if(Session::has('mensaje'))
<div class="alert alert-success alert-dismissible" role="alert">
{{Session::get('mensaje') }}
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif


<a href="{{url('usuario/create')}}"  class="btn btn-dark" >Nuevo registro</a>
</br>
</br>
<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Correo</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($usuarios as $usuario)
        <tr>
            <td>{{$usuario->id }}</td>

            <td>
            <img class="img-thumbnail img-fluid" src='{{asset("storage/$usuario->Foto")}}'  width="100" alt="">
            
            </td>

            
            <td>{{$usuario->Nombre }}</td>
            <td>{{$usuario->Apellidos}}</td>
            <td>{{$usuario->Correo}}</td>
            <td>
            
            <a href="{{url('/usuario/'.$usuario->id.'/edit')}}" class="btn btn-warning">
            Editar 
            </a>
         
            
            <form action="{{url('/usuario/'.$usuario->id) }}" class="d-inline" method="post">
            @csrf
            {{ method_field('DELETE') }}
             <input class="btn btn-danger" type="submit" onclick="return confirm('Deseas borrar el usuario?')" value="Borrar">

            </form>
            </td>
            
        </tr>
        @endforeach
        
    </tbody>
</table>
{!! $usuarios->links() !!}
</div>
@endsection