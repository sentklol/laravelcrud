<h1> {{$modo}} Usuario </h1>

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">
<ul>

        @foreach( $errors->all() as $error)
       <li> {{ $error }} </li>
    @endforeach
</ul>   

    </div>


@endif




<div class="form-group">
<label for="Nombre">  NOMBRE </label>
<input type="text" class="form-control" name="Nombre" value="{{isset($usuario->Nombre)?$usuario->Nombre:old('Nombre')}}" id="Nombre">
</div>

<div class="form-group">
<label for="Apellidos">  APELLIDOS </label>
<input type="text" class="form-control" name="Apellidos" value="{{isset($usuario->Apellidos)?$usuario->Apellidos:old('Apellidos')}}" id ="Apellidos">
</div>

<div class="form-group">
<label for="Correo">  CORREO </label>
<input type="text" class="form-control" name="Correo" value="{{isset($usuario->Correo)?$usuario->Correo:old('Correo')}}" id="Correo">
</div>

<div class="form-group">
<label for="Foto"></label>
@if(isset($usuario->Foto))
<img class="img-thumbnail img-fluid" src="{{asset('storage').'/'.$usuario->Foto }}" width="100" alt="">
@endif 
<input type="file" class="form-control" name="Foto" value="" id="Foto">
</div>

<input class="btn btn-success" type="submit" value="{{$modo}} datos">
<a class="btn btn-outline-info" href="{{url('usuario/')}}">Regresar</a>

<br>
