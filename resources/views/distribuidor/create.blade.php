@extends('layouts.app')
@section('content')
<div class="container">

<form action="{{ url('/distribuidor') }}" method="post" enctype="multipart/form-data" >
@csrf
@include('distribuidor.form',['modo'=>'Crear'])
</form>
</div>
@endsection