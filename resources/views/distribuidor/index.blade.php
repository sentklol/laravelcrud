@extends('layouts.app')
@section('content')
<div class="container">

@if(Session::has('mensaje'))
<div class="alert alert-success alert-dismissible" role="alert">
{{Session::get('mensaje') }}
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif


<a href="{{url('distribuidor/create')}}"  class="btn btn-dark" >Nuevo registro</a>
</br>
</br>
<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Logo</th>
            <th>Nombre</th>
            <th>Ciudad</th>
            <th>Correo</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($distribuidores as $distribuidor)
        <tr>
            <td>{{$distribuidor->id }}</td>

            <td>
            <img class="img-thumbnail img-fluid" src="{{asset('storage').'/'.$distribuidor->Logo }}"  width="100" alt="">
            
            </td>

            
            <td>{{$distribuidor->Nombre }}</td>
            <td>{{$distribuidor->Ciudad}}</td>
            <td>{{$distribuidor->Correo}}</td>
            <td>
            
            <a href="{{url('distribuidor/'.$distribuidor->id.'/edit')}}" class="btn btn-warning">
            Editar 
            </a>
         
            
            <form action="{{url('distribuidor/'.$distribuidor->id) }}" class="d-inline" method="post">
            @csrf
            {{ method_field('DELETE') }}
             <input class="btn btn-danger" type="submit" onclick="return confirm('Deseas borrar el distribuidor?')" value="Borrar">

            </form>
            </td>
            
        </tr>
        @endforeach
        
    </tbody>
</table>
<!-- {!! $distribuidores->links() !!} -->
</div>
@endsection