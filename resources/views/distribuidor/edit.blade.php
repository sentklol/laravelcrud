@extends('layouts.app')
@section('content')
<div class="container">
<form action="{{url('/distribuidor/'.$distribuidor->id)}}" method="post" enctype="multipart/form-data">
@csrf 
{{method_field('PATCH')}}
@include('distribuidor.form',['modo'=>'Editar'])

</form>
</div>
@endsection
