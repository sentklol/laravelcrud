<h1> {{$modo}} distribuidor </h1>

@if(count($errors)>0)

    <div class="alert alert-danger" role="alert">
<ul>

        @foreach( $errors->all() as $error)
       <li> {{ $error }} </li>
    @endforeach
</ul>   

    </div>


@endif




<div class="form-group">
<label for="Nombre">  NOMBRE </label>
<input type="text" class="form-control" name="Nombre" value="{{isset($distribuidor->Nombre)?$distribuidor->Nombre:old('Nombre')}}" id="Nombre">
</div>

<div class="form-group">
<label for="Ciudad">  CIUDAD </label>
<input type="text" class="form-control" name="Ciudad" value="{{isset($distribuidor->Ciudad)?$distribuidor->Ciudad:old('Ciudad')}}" id ="Ciudad">
</div>

<div class="form-group">
<label for="Correo">  CORREO </label>
<input type="text" class="form-control" name="Correo" value="{{isset($distribuidor->Correo)?$distribuidor->Correo:old('Correo')}}" id="Correo">
</div>

<div class="form-group">
<label for="Logo"></label>
@if(isset($distribuidor->Logo))
<img class="img-thumbnail img-fluid" src="{{asset('storage').'/'.$distribuidor->Logo }}" width="100" alt="">
@endif 
<input type="file" class="form-control" name="Logo" value="" id="Logo">
</div>

<input class="btn btn-success" type="submit" value="{{$modo}} datos">
<a class="btn btn-outline-info" href="{{url('distribuidor/')}}">Regresar</a>

<br>
